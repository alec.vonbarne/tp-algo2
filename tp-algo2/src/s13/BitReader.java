package s13;

import java.io.*;

// Reads any file one bit at a time (most significant bit first)
public class BitReader {
  private final FileInputStream fis;
  private byte currentByte;
  private int numberOfBit = 0;

  public BitReader(String filename) throws IOException {
    fis= new FileInputStream(filename);
  }

  public void close() throws IOException {
    fis.close();
  }

  public boolean next() throws IOException {
    int i = numberOfBit % 8;
    if (i == 0) {
      currentByte = (byte) fis.read();
    }
    numberOfBit++;
    return (currentByte & (1 << (7 - i))) != 0;
  }

  public boolean isOver() {
    int remaining = 0;
    try {
      remaining = fis.available();
    } catch (IOException e) {
      // do something evil
      e.printStackTrace();
    }
    return numberOfBit % 8 == 0 && remaining == 0;
  }

  //-------------------------------------------
  // Tiny demo...
  public static void main(String [] args) {
    String filename="a.txt";
    try {
      BitReader b = new BitReader(filename);
      int i=0;
      while(!b.isOver()) {
        System.out.print(b.next()?"1":"0");
        i++;
        if (i%8  == 0) System.out.print(" ");
        if (i%80 == 0) System.out.println("");
      }
      b.close();
    } catch (IOException e) {
      throw new RuntimeException(""+e);
    }
  }
}
