package s11;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;


public class testIntStackJU {

    @Test
    public void testIntStack() {
        IntStack s = new IntStack();

        // check if empty, should be empty at start
        assertTrue(s.isEmpty());

        s.push(1);
        s.push(2);

        assertEquals(2, s.top());
        assertEquals(2, s.pop());

        Random r = new Random();

        /*for (int i = 0; i < 100; i++) {
            s.push(r.nextInt());
        }*/
    }
}

// Le test permet d'atteindre 100% de line coverage
// Mais le test passe et ne permet pas de trouver l'erreur

// 4. a Il faut choisir dans les configurations d'éxecutions
// https://www.jetbrains.com/help/idea/running-test-with-coverage.html#coverage-run-configurations
// sampling -> line coverage
// tracing -> branch coverage
// 4. b ça veut dire qu'une partie du if n'a pas été executée car la condition était toujours true ou false
