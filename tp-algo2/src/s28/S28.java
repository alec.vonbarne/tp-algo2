
package s28;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.stream.*;

public class S28 {

  // ------ Ex. 1 -----------
  public static void printSomePowersOfTwo() {
    IntStream.rangeClosed(0,10)
            .map(s -> (int)Math.pow(2, s))
            .forEach(System.out::println);
  }

    // ------ Ex. 2 -----------
  public static String macAddress(int[] t) {
   String k = Arrays.stream(t)
            .mapToObj(s -> String.format("%02X", s))
            .collect(Collectors.joining (":"));

    return k;
  }

  // ------ Ex. 3 -----------

  public static long[] divisors(long n) {
    long[] l = LongStream.rangeClosed(1, n-1)
            .filter(s -> n % s == 0)
            .toArray();

    return l;
  }

  // ------ Ex. 4a -----------
  public static long sumOfDivisors(long n) {
    return LongStream.of(divisors(n)).sum();
  }


  // ------ Ex. 4b -----------
  public static long[] perfectNumbers(long max) {
    return LongStream.rangeClosed(0, max)
            .filter(s -> s == sumOfDivisors(s))
            .toArray();
  }

  // ------ Ex. 5 -----------

  public static void printMagicWord() throws IOException {
    Path path = FileSystems.getDefault().getPath("wordlist.txt");
    Files.lines(path)
      .filter(x -> x.length() == 11)
      .filter(x -> x.chars().distinct().count() == 6)
      .filter(x -> x.charAt(2) == 't')
      .filter(x -> x.charAt(4) == 'l')
      .forEach(System.out::println);
   }

  public static boolean isPalindrome(String str) {
    return str.equals(new StringBuilder(str).reverse().toString());
  }

  public static void printPalindromes() throws IOException {
    Path path = FileSystems.getDefault().getPath("wordlist.txt");
    Files.lines(path)
            .filter(s -> s.length() >= 4)
            .filter(s -> s.equals(new StringBuilder(s).reverse().toString()))
            .forEach(System.out::println);
  }

  // ------ Ex. 6 -----------
  public static void averages() {
    double[][] results = {
        { 9, 10,  8,  5,  9},
        { 5,  9,  9,  8,  8},
        { 4,  8, 10,  9,  5},
        { 8, 10,  8, 10,  7},
        { 8,  9,  7, 10,  6},
    };

    double[] grades = new double[results.length];
    for (int i = 0; i < results.length; i++) {
        double sum = 0;
        for (int j = 0; j < results[i].length; j++) {
            sum += results[i][j];
        }
        grades[i] = 1.0 + sum / 10.0;
    }
    double sum = 0;
    for (int i = 0; i < grades.length; i++) {
        sum += grades[i];
    }
    double average = sum / grades.length;

    System.out.printf("Grades : %s%n", Arrays.toString(grades));
    System.out.printf("Average: %.2f%n", average);
  }

  public static void averagesWithStreams() {
    double[][] results = {
        { 9, 10,  8,  5,  9},
        { 5,  9,  9,  8,  8},
        { 4,  8, 10,  9,  5},
        { 8, 10,  8, 10,  7},
        { 8,  9,  7, 10,  6},
    };

    double[] grades = Arrays.stream(results).mapToDouble(s -> (Arrays.stream(s).sum() / 10 + 1)).toArray();
    double average = Arrays.stream(grades).sum() / Arrays.stream(grades).count();

    System.out.printf("Stream - Grades : %s%n", Arrays.toString(grades));
    System.out.printf("Stream - Average: %.2f%n", average);
  }
  
  // ------ Ex. 7 -----------
  /*
  a) => Stream.of()	b) => IntStream.rangeClosed()	c) => Stream.map()	d) => Stream.reduce()
  
  génératives : Arrays.stream(), IntStream.mapToDouble(), IntStream.rangeClosed(), Stream.of()
  intermédiaires : Stream.filter, Stream.map()
  terminales : Stream.reduce(), DoubleStream.sum(), DoubleStream.max()
  */
  
  // ------ Ex. 8 -----------

  static DoubleStream sampling(double from, double to, int nSubSamples) {
    return null; // TODO ...
  }

  public static void ex8() {
    Double[] d = {1.2, 3.4, 5.6};
    System.out.println(Arrays.stream(d).reduce(1.0, (a, b) -> a * b));

    System.out.println(IntStream.rangeClosed(0, 20).map(i -> (int) (i / (Math.pow(2,i)))).sum());
            //... math series:  i/2^i for i between 0 and 20

    String[] s = {"Hello","World","!"};
    System.out.println(Arrays.stream(s).collect(Collectors.joining("")));
            // ... concatenation of: {"Hello","World","!"}
  
    System.out.println(DoubleStream.iterate(0, i -> i + Math.PI/1001)
            .limit(1002)
            .map(x -> Math.pow(Math.sin(x), 2.0) * Math.cos(x) )
            .max());
    
    /*DoubleStream doubleStream = sampling(0, Math.PI, 1002); // TODO - Sûrement faire qqch du style plutôt
    assert doubleStream != null;
    System.out.println(doubleStream
            .map(x -> Math.pow(Math.sin(x), 2.0) * Math.cos(x) )
            .max());*/
  }

  // ------ Ex. 9 -----------
  public static void nipas(int n) {
    System.out.println(
      IntStream.range(0, n)
      .mapToObj(
        i -> IntStream.range(i, i+4)
             .mapToObj(j ->
               new String(new char[n+2-j]).replace("\0", " ") +
               new String(new char[1+2*j]).replace("\0", "*"))
             .collect(Collectors.joining("\n")))
      .collect(Collectors.joining("\n"))
    );
  }

  //-------------------------------------------------------
  public static void main(String[] args) throws IOException {
    printSomePowersOfTwo();

    System.out.println(macAddress(new int[]{78, 26, 253, 6, 240, 13}));

    System.out.println(Arrays.toString(divisors(496L)));

    System.out.println(sumOfDivisors(496L));

    System.out.println(Arrays.toString(perfectNumbers(10000L)));

    try {
      printMagicWord();
      printPalindromes();
    } catch(IOException e) {
      System.out.println("!! Problem when reading file... "+e.getMessage());
    }

    averages();
    averagesWithStreams();
    
    ex8();

    // nipas(4); // read/analyze the code first!...
  }

}
