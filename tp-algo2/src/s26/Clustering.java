package s26;
import s22.ClosestPair;

import java.awt.Point;
import java.util.*;

public class Clustering {
  static Random rnd = new Random();

  static void shuffleRandomly(int[] t) {
    int aux, j;
    for (int i=1; i<t.length; i++) {
      j = rnd.nextInt(i+1);
      aux = t[i]; t[i]=t[j]; t[j]=aux;
    }
  }

  /** Randomly selects one of the points to be the center of each cluster */
  public static List<Cluster> initKMeans(List<Point> pts, int nbOfClusters) {
    int n = pts.size();
    assert(nbOfClusters < n);
    List<Cluster> clusters = new ArrayList<Cluster>();
    int[] t = new int [n];
    for (int i=0; i<n; i++) t[i]=i;
    shuffleRandomly(t);
    for (int i=0; i<nbOfClusters; i++) {
      clusters.add(new Cluster(Arrays.asList(pts.get(t[i%n]))));
    }
    return clusters;
  }

  private static Cluster closestCluster(List<Cluster> clusters, Point p) {
    double closestDist = Double.POSITIVE_INFINITY;
    Cluster closest = null;
    for(Cluster c: clusters) {
      double d = p.distance(c);
      if (d<closestDist) { 
        closestDist=d; closest=c; 
      }
    }
    return closest;
  }

  public static void kMeans(List<Cluster> clusters, List<Point> pts) {
    boolean stable = true;    
    do {
      for (Cluster c : clusters) c.removeAllMembers();
      for (Point p : pts) closestCluster(clusters, p).addPoint(p);
      for (Cluster c : clusters) stable = !c.recomputeCenter();
    } while (!stable);
  }

  public static List<Cluster> initHierarchical(List<Point> pts) {
    List<Cluster> clusters = new ArrayList<>();
    for (Point p : pts) clusters.add(new Cluster(Collections.singletonList(p)));
    return clusters;
  }
  
  public static void hierarchical(List<Cluster> clusters, int nbOfClusters) {
    while (clusters.size() != nbOfClusters) {
      Point[] closestPair = ClosestPair.closestPair(clusters);
      clusters.removeAll(Arrays.asList(closestPair));
      clusters.add(new Cluster((Cluster) closestPair[0], (Cluster) closestPair[1]));
    }
  }
}

