
package s26;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

/** Here, Cluster is a subclass of Point (a cluster "is-a" center of mass).
 * It's rather  strange, but with a small advantage when trying to reuse
 * a method in order to find the "two closest clusters" among a group
 * of clusters... 
 */
@SuppressWarnings("serial")
public class Cluster extends Point {
  List<Point> points;

  @Override public String toString() {
    return "[" + points.size() + " points, centered on " + x + " " + y + "]";
  }

  /** copies the points found in clusters a and b */
  public Cluster(Cluster a, Cluster b) {
    this(a.points);
    points.addAll(b.points);
    recomputeCenter();
  }

  public Cluster(List<Point> points) {
    this.points = new ArrayList<Point>(points);
    recomputeCenter();
  }

  void addPoint(Point p) {
    points.add(p); 
  }

  void removeAllMembers() {
    points = new ArrayList<Point>();
  }

  /** Recomputes the center of mass (this.x, this.y) from the points  
   *  @return: whether the coordinates have changed 
   *           since the last call of the method
   */  
  public boolean recomputeCenter() {
    int oldX = x, oldY = y;
    x = 0; 
    y = 0;
    int n = points.size();
    for (Point p: points) {
      x += p.x; 
      y += p.y;
    }
    if (n != 0) {
      x /= n; 
      y /= n;
    }
    return (oldX != x || oldY != y);
  } 
}