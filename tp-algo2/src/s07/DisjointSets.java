package s07;

import java.util.Arrays;
// ------------------------------------------------------------
public class DisjointSets {

  private int[] holder;
  private int nbrOfSets;

  public DisjointSets(int nbOfElements) {
    this.holder = new int[nbOfElements];

    this.nbrOfSets = nbOfElements;

    for (int i = 0; i < nbOfElements; i++) {
      this.holder[i] = -1;
    }
  }

  private int getParentIndex(int i) {
    int iIndex = i;
    while (this.holder[iIndex] > -1) {
      iIndex = this.holder[iIndex];
    }

    return iIndex;
  }

  public boolean isInSame(int i, int j) {

    return getParentIndex(i) == getParentIndex(j);
  }

  public void union(int i, int j) {
    j = getParentIndex(j);
    i = getParentIndex(i);

    if (i == j || isInSame(i, j)) {
      return;
    }

    this.holder[i] += this.holder[j];
    this.holder[j] = i;
    this.nbrOfSets--;
  }

  public int nbOfElements() {  // as given in the constructor

    return this.holder.length;
  }

  public int minInSame(int i) {
    int min = i;

    while (i != -1) {
      if (i < min) {
        min = i;
      }
      i = this.holder[i];
    }

    return min;
  }

  public boolean isUnique() {

    return this.nbrOfSets == 1;
  }

  @Override
  public String toString() {
    return Arrays.toString(this.holder);
  }

  @Override
  public boolean equals(Object otherDisjSets) {
    if (otherDisjSets == null) return false;

    DisjointSets other;
    try {
      other = (DisjointSets) otherDisjSets;
    } catch (Exception e) {
      return false;
    }

    if (this.holder.length != other.holder.length) return false;

    boolean inThis, inOther;
    for (int i = 0; i < holder.length; i++) {
      for (int j = i - 1; j >= 0; j--) {
        inThis = this.isInSame(i, j);
        inOther = other.isInSame(i, j);
        if (inThis != inOther) return false;
        if (inThis) break;
      }
    }

    return true;
  }
}
