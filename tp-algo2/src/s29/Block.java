
package s29;

import java.util.Arrays;

// In this simplified model, the "difficulty" parameter for the mining task has
// a very coarse granularity: it tells how many '0' characters must be present
// at the beginning of the sha256(). The 8x finer granularity would be to count
// the number of the leading 0 bits (and not bytes).

public class Block {
  // mining-independent fields:
  public final long index;
  public final String previousHash;
  public final String data;

  // mining-dependent field:
  private long miningNonce;

  public Block(int index, String prevHash, String data) {
    this.index = index;
    this.previousHash = prevHash;
    this.data = data;
    this.miningNonce = 0;
  }

  public String sha256() {
    String allContents = toString();
    return CryptoUtils.sha256(allContents);
  }

  private static String targetPrefix(int difficulty) {
    char[] zeros = new char[difficulty];
    Arrays.fill(zeros, '0');
    return new String(zeros);
  }

  /** Changes miningNonce so that this.sha256() starts with k=difficulty '0's  */
  public void mineBlock(int difficulty) {
    while (true) {
      if (!isMined(difficulty)) {
        miningNonce++;
      } else {
        return;
      }
    }
  }

  public boolean isMined(int difficulty) {
    String dest = "";
    for (int i = 0; i < difficulty; i++) {
      dest += "0";
    }

    return sha256().startsWith(dest);
  }

  public void checkValidity(int supposedDifficulty, Block supposedPreviousBlock)
					 throws BadBlockchainException {
    if (supposedPreviousBlock == null) {
      return;
    }
    if (!supposedPreviousBlock.isMined(supposedDifficulty)) {
      throw new BadBlockchainException("");
    }
  }

  @Override
  public String toString() {
    return String.format("%d %s %s %d", index, previousHash, data, miningNonce);
  }

  public String toReadableString() {
    StringBuilder sb = new StringBuilder();
    sb.append(String.format(" BlockIndex: %d \n", index));
    sb.append(String.format("   PrevHash: %s \n", previousHash));
    sb.append(String.format("       Data: %s \n", data));
    sb.append(String.format("      Nonce: %d \n", miningNonce));
    sb.append(String.format("CurrentHash: %s \n", sha256()));
    return sb.toString();
  }

}
