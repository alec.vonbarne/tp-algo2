
package s29;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

// Here, money is not in "dollars", but in "bolivars".

// The BolivarBank is very generous/silly: every new account mentioned in a
// transfer is automatically created with a certain amount of money (a
// welcome present). But in our bank it is forbidden to have a negative balance.

public class BolivarBank {
  public final long INITIAL_WELCOME_GIFT = 100;

  /** Account -> remainingBolivars  */
  private final Map<String, Long> balance = new HashMap<>();

  /** Account -> number of transactions already done by the owner  */
  private final Map<String, Long> nTransactions = new HashMap<>();

  public void createIfUnknownAccount(String account) {
    if(balance.containsKey(account)) return;
    balance.put(account, INITIAL_WELCOME_GIFT);
    nTransactions.put(account, 0L);
  }

  public long moneyOf(String account) {
    createIfUnknownAccount(account);
    return balance.get(account);
  }

  /** Applies the transaction or else throws an exception if that
   *  transaction is not reasonable                                    */
  private void applyTransaction(Transaction t) throws BadTransactionException {
    createIfUnknownAccount(t.movement.receiverAccount);
    t.checkSignature();

    if (moneyOf(t.movement.senderAccount) < t.movement.amount) {
      throw new BadTransactionException("");
    }

    Long rec = nTransactions.get(t.movement.receiverAccount)+1;
    Long sender = nTransactions.get(t.movement.senderAccount)+1;

    //nTransactions.remove(t.movement.receiverAccount);
    //nTransactions.remove(t.movement.senderAccount);

    nTransactions.put(t.movement.receiverAccount, rec);
    nTransactions.put(t.movement.senderAccount, sender);

    rec = balance.get(t.movement.receiverAccount);
    sender = moneyOf(t.movement.senderAccount);

    //balance.remove(t.movement.receiverAccount);
    //balance.remove(t.movement.senderAccount);

    balance.put(t.movement.receiverAccount, rec + t.movement.amount);
    balance.put(t.movement.senderAccount, sender - t.movement.amount);
  }

  public void runFullStory(List<Transaction> ts) throws BadTransactionException {
    for(Transaction t: ts)
      applyTransaction(t);
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    for(String account: balance.keySet()) {
      sb.append("Account: " + account + "\n");
      sb.append("        balance: " + moneyOf(account) + "\n");
      sb.append("  #transactions: " + nTransactions.get(account) + "\n");
    }
    return sb.toString();
  }

}
