package s21;
import java.awt.Point;

public class Geom {

  public static int signedArea(Point p1, Point p2, Point p3) {
    return (p2.x-p1.x)*(p3.y-p1.y) - (p3.x-p1.x)*(p2.y-p1.y);
    // negative if clockwise; twice the area of the triangle p1-p2-p3
  }

  public static int ccw(Point p1, Point p2, Point p3) {
    int a = signedArea(p1, p2, p3);
    if (a < 0) return -1;
    if (a > 0) return +1;
    return 0;
  }

  public static boolean intersect(Segm s0, Segm s1) {
    int firstCheck = ccw(s0.from(), s0.to(), s1.from()) * ccw(s0.from(), s0.to(), s1.to());
    int secondCheck = ccw(s1.from(), s1.to(), s0.from()) * ccw(s1.from(), s1.to(), s0.to());
    if (firstCheck == 0 && secondCheck == 0) {
      return (isOnSegment(s0.from(), s1) || isOnSegment(s0.to(), s1) ||
              isOnSegment(s1.from(), s0) || isOnSegment(s1.to(), s0));
    }
    return (firstCheck <= 0) && (secondCheck <= 0);
  }

  private static boolean isInAngle(Point query, Point a, Point b, Point c) {
    int abc = ccw(a, b, c);
    return ((abc * ccw(a, query, b) <= 0) && (abc * ccw(b, query, c) <= 0));
  }

  public static boolean isInLeftAngle(Point query, Point a, Point b, Point c) {
    int abc = ccw(a, b, c);
    return (abc <= 0) != isInAngle(query, a, b, c);
  }

  public static boolean isOnSegment(Point p, Segm s) {
    if (p.x > s.to().x || p.x < s.from().x || p.y > s.to().y || p.y < s.from().y) {
      return false;
    }
    return signedArea(p, s.from(), s.to()) <= 0;
  }

  public static boolean isInCcwOrder(Point [] simplePolygon) {
    int sum = 0;
    for (int i = 0; i < simplePolygon.length; i++) {
      int newI = (i + 1) % simplePolygon.length;
      sum += (simplePolygon[newI].x-simplePolygon[i].x)*(simplePolygon[newI].y+simplePolygon[i].y);
    }
    return (sum < 0);
  }

  public static boolean isInPolygon(Point [] polyg, Point p) {
    for (int i = 0; i < polyg.length; i++) {
      if (!isInAngle(p, polyg[i], polyg[((i+1)%polyg.length)], polyg[((i+2)%polyg.length)])) {
        return false;
      }
    }
    return true;
  }

  public static void main(String[] args) {
    s21.CG.main(args);
  }

}
