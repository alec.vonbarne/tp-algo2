package s10;

import java.util.BitSet;
import java.util.Random;
import org.junit.Test;
import static org.junit.Assert.*;

public class TreapTestJU {
  public int add, not_add, delete, not_delete, add_left, add_right, remove_left, remove_right;
  public int rotateLeft;
  public int rotateRight;
  // ------------------------------------------------------------
  @Test
  public void testTreap() {
    int n=2000;
    testSet(n);
  }
  // ------------------------------------------------------------
  static void rndAddRm(Random r, Treap<Integer> s, BitSet bs, int i) {
    if (r.nextBoolean()) { 
      s.add(i); bs.set(i);
      //System.out.println("add "+i+": "+s+" // "+bs);
      //if (! areSetEqual(s, bs)) {
      //System.out.println("oh ! add "+i);
      //}
    } else {
      s.remove(i); bs.clear(i);
      //System.out.println("rm  "+i+": "+s+" // "+bs);
      //if (! areSetEqual(s, bs)) {
      //System.out.println("oh ! rm "+i);
      //}
    }
  }
  // ------------------------------------------------------------
  boolean areSetEqual(Treap<Integer> s, BitSet bs) {
    int l=0;
    for (int i=0; i<bs.length(); i++) {
      if(bs.get(i) != s.contains(Integer.valueOf(i))) {
        System.out.println("SetOf : "+s);
        System.out.println("BitSet: "+bs);
        System.out.println("Size: "+s.size());
        System.out.println("missing element : " +i);
        return false;
      }
      if (s.contains(i))
        l++;
    }
    if (l != s.size()) {
      System.out.println("SetOf : "+s);
      System.out.println("BitSet: "+bs);
      System.out.println("Size: "+s.size());
      System.out.println("too much elements...");
      return false;
    }
    return true;
  }
  // ------------------------------------------------------------
  // testSet : Simple test method for the Set specification. 
  //           It only verifies that an arbitrary sequence of add/remove
  //           results in a correct set. 
  //     prm : n is the maximum size of the tested set (typically 1000).
  
  public void testSet(int n) {
    Treap<Integer> s = new Treap<Integer>();
    this.add = 0;
    this.not_add = 0;
    this.delete = 0;
    this.not_delete = 0;
    this.add_left = 0;
    this.add_right = 0;
    this.remove_left = 0;
    this.remove_right = 0;
    this.rotateLeft = 0;
    this.rotateRight = 0;
    
    int m=2*n;
    BitSet bs = new BitSet();
    Random r = new Random();
    for (int i=0; i<10; i++) {
      assertTrue(areSetEqual(s, bs));
      rndAddRm(r, s, bs, r.nextInt(m));
    }
    while(bs.cardinality()<n) {
      rndAddRm(r, s, bs, r.nextInt(m));
      if (bs.cardinality()%10==0)
        assertTrue(areSetEqual(s, bs));
      int j=r.nextInt(m); 
      s.add(j); 
      bs.set(j);
      //System.out.print(".");
      if (s.add > (this.add + this.not_add)) {
        if (s.rotateLeft > this.rotateLeft || s.rotateRight > this.rotateRight) {
          this.add_right += s.rotateRight - this.rotateRight;
          this.add_left += s.rotateLeft - this.rotateLeft;
          this.add = s.add - this.not_add;
        } else {
          this.not_add = s.add - this.not_delete;
        }
      }
      if (s.delete > (this.delete + this.not_delete)) {
        if (s.rotateLeft > this.rotateLeft || s.rotateRight > this.rotateRight) {
          this.remove_right += s.rotateRight - this.rotateRight;
          this.remove_left += s.rotateLeft - this.rotateLeft;
          this.delete = s.delete - not_delete;
        } else {
          this.not_delete = s.delete - this.delete;
        }
      }
      this.rotateLeft = s.rotateLeft;
      this.rotateRight = s.rotateRight;
    }
    assertTrue(areSetEqual(s, bs));
    System.out.printf("add:%d->%d \t= letf:%d/right:%d \t/ remove:%d->%d   \t= letf:%d/right:%d\n",
            s.add, this.add, this.add_left, this.add_right, s.delete, this.delete, this.remove_left, this.remove_right);
  }
  // ------------------------------------------------------------
}
