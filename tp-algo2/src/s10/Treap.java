package s10;
import java.util.PriorityQueue;
import java.util.Random;
//--------------------------------------------------
public class Treap<E extends Comparable<E>>  {
  //============================================================
  static class TreapElt<E extends Comparable<E>> implements Comparable<TreapElt<E>> {
    static Random rnd=new Random();
    // -----------------------
    private final E elt;
    private int     pty;
    // -----------------------
    public TreapElt(E e) {
      elt=e;
      pty=rnd.nextInt();
    }

    public int pty() {
      return pty;
    }

    public E elt() {
      return elt;
    }

    public int compareTo(TreapElt<E> o) {
      return elt.compareTo(o.elt);
    }

    @Override public boolean equals(Object o) {
      if(o==null) return false;
      if (this.getClass() != o.getClass()) return false;
      if (elt==null) return false;
      return elt.equals(((TreapElt<?>)o).elt);
    }

    @Override public String toString() {
      return ""+elt+"#"+pty;
    }

    @Override public int hashCode() {
      return elt.hashCode();
    }
  }
  //============================================================
  private final BST<TreapElt<E>> bst;
  private int size;
  private TreapElt min;
  private TreapElt max;

  // for stats
  public int add;
  public int delete;
  public int rotateLeft;
  public int rotateRight;

  // --------------------------
  public Treap() {
    bst= new BST<TreapElt<E>>();
  }

  public void add(E e) {
    add++;
    if (contains(e)) { return; }
    TreapElt elt = new TreapElt(e);
    bst.add(elt);
    BTreeItr itr = bst.locate(elt);
    percolateUp(itr);
    size++;

    if (size == 1) {
      min = elt;
      max = elt;
    } else {
      if (elt.compareTo(min) < 0) {
        min = elt;
      }
      if (elt.compareTo(max) > 0) {
        max = elt;
      }
    }
  }

  public void remove(E e) {
    delete++;
    if (!contains(e)) {return;}
    BTreeItr itr = locate(e);
    siftDownAndCut(itr);
    size--;

    if (e.equals(min.elt)) {
      min = mostRight();
    }

    if (e.equals(max.elt)) {
      max = mostLeft();
    }
  }

  public boolean contains(E e) {
    return !locate(e).isBottom();
  }

  public int size() {
    return size;
  }

  public E minElt() {
    return bst.minElt().elt;
  }

  public E maxElt() {
    return bst.maxElt().elt;
  }

  public String toString() {
    return bst.toString();
  }

  // --------------------------------------------------
  // --- Non-public methods
  // --------------------------------------------------
  private void siftDownAndCut(BTreeItr<TreapElt<E>> ti) {
    while (!ti.isLeafNode()) {
      if (ti.hasRight()) {
        if (ti.hasLeft() && isLess(ti.left(), ti.right())) {
          rotateRight++;
          ti.rotateRight();
          ti = ti.right();
        } else {
          rotateLeft++;
          ti.rotateLeft();
          ti = ti.left();
        }
      } // should not rotate right, so don't need to do the checks
      if (ti.hasLeft()) {
        rotateRight++;
        ti.rotateRight();
        ti = ti.right();
      }
    }
    ti.cut();
  }

  private void percolateUp(BTreeItr<TreapElt<E>> ti) {
    while((!ti.isRoot()) && isLess(ti, ti.up())) {
      if (ti.isLeftArc()) {ti=ti.up(); ti.rotateRight();rotateRight++;}
      else                {ti=ti.up(); ti.rotateLeft(); rotateLeft++;}
    }
  }

  private BTreeItr locate(E e) {
    BTreeItr<TreapElt<E>> itr = bst.tree.root();
    while (!itr.isBottom()) {
      E c = itr.consult().elt;
      if (e.compareTo(c) == 0) break;
      if (e.compareTo(c) < 0) itr = itr.left();
      else itr = itr.right();
    }
    return itr;
  }

  private TreapElt<E> mostRight() {
    BTreeItr<TreapElt<E>> itr = bst.tree.root();
    while (itr.hasRight()) itr = itr.right();

    return itr.consult();
  }

  private TreapElt<E> mostLeft() {
    BTreeItr<TreapElt<E>> itr = bst.tree.root();
    while (itr.hasLeft()) itr = itr.left();

    return itr.consult();
  }

  private boolean isLess(BTreeItr<TreapElt<E>> a, BTreeItr<TreapElt<E>> b) {
    TreapElt<E> ca= a.consult();
    TreapElt<E> cb= b.consult();
    return ca.pty()<cb.pty();
  }
}
