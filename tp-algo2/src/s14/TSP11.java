package s14;
//======================================================================
public class TSP11 implements TSP {
  public void salesman(TSPPoint [] t, int [] path) {
    int i, j;
    int n = t.length;
    boolean[] visited = new boolean[n];
    int thisPt, closestPt = 0;
    double shortestDist;
    
    thisPt = n-1;
    if (thisPt < 0) return;
    visited[thisPt] = true;
    path[0] = n-1;  // chose the starting city
    for(i=1; i<n; i++) {
      shortestDist = Double.MAX_VALUE;
      double p1X = t[thisPt].x, p1Y = t[thisPt].y; // avoid j access to the array t
      for(j=0; j<n; j++) {
        if (visited[j])
          continue;
        
        double tempDistXsqr = p1X - t[j].x, tempDistY = p1Y - t[j].y;
        double tempDist = tempDistXsqr*tempDistXsqr + tempDistY*tempDistY;
        
        if (tempDist < shortestDist ) {
          shortestDist = tempDist;
          closestPt = j;
        }
      }
      path[i] = closestPt;
      visited[closestPt] = true;
      thisPt = closestPt;
    }
  }
  //----------------------------------
  static private double sqr(double a) {
    return a*a;
  }
  //---------------------------------
  static private double distance(TSPPoint p1, TSPPoint p2) {
    return Math.sqrt(sqr(p1.x-p2.x) + sqr(p1.y-p2.y));
  }
}
