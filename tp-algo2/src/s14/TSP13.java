package s14;
//======================================================================
public class TSP13 implements TSP {
  public void salesman(TSPPoint [] t, int [] path) {
    int i, j;
    int n = t.length;
    int thisPt, closestPt = 0, closestCity = 0, tempClosestPt, firstCity, actualCity;
    double shortestDist;
    
    // creation of a table that will avoid access to the old boolean table named visited
    firstCity = n-1;
    if (firstCity < 0) return;
    int [] cities = new int[firstCity];
    for (int k = 0; k < firstCity; k++)
      cities[k] = k;
  
    thisPt = firstCity;
    double p1X, p1Y, tempDistXsqr, tempDistY, tempDist; // an affectation is more optimized than a declaration
    
    for(i=0; i<firstCity; i++) {
      path[i] = thisPt;
      shortestDist = Double.MAX_VALUE;
      p1X = t[thisPt].x; p1Y = t[thisPt].y; // avoid j access to the array t
      
      for(j=i; j<firstCity; j++) {
        actualCity = cities[j];
        
        tempDistXsqr = p1X - t[actualCity].x; tempDistY = p1Y - t[actualCity].y;
        tempDistXsqr = tempDistXsqr*tempDistXsqr;
        
        if (tempDistXsqr > shortestDist) // avoid unnecessary affectation of variable and compute of values
          continue;
        tempDist = tempDistXsqr + tempDistY*tempDistY;
        
        if (tempDist < shortestDist ) {
          shortestDist = tempDist;
          closestPt = actualCity;
          closestCity = j;
        }
      }
      thisPt = closestPt;
      // swap the closestCity index with the index i
      tempClosestPt = cities[i];
      cities[i] = closestCity;
      cities[closestCity] = tempClosestPt;
    }
    path[firstCity] = closestPt;
  }
  
  //----------------------------------
  static private double sqr(double a) {
    return a*a;
  }
  //---------------------------------
  static private double distance(TSPPoint p1, TSPPoint p2) {
    return Math.sqrt(sqr(p1.x-p2.x) + sqr(p1.y-p2.y));
  }
}
