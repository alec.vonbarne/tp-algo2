package s09;

// ------------------------------------------------------------
public class StringSearching {
  // ------------------------------------------------------------
  static /*final*/ int HASHER = 301; // Maybe also try with 7 and 46237
  static /*final*/ int BASE   = 256; // Please also try with 257
  // ---------------------
  static int firstFootprint(String s, int len) {
    int fp = 0;

    for (int i = 0; i < len; i++) {
      fp = (BASE * fp + s.charAt(i)) % HASHER;
    }
    return fp;
  }
  // ---------------------
  // must absolutely be O(1)
  // coef is (BASE  power  P.LENGTH-1)  mod  HASHER
  static int nextFootprint(int previousFootprint, char dropChar, char newChar, int coef) {
    int h = previousFootprint;

    h = (BASE * (h - dropChar * coef) + newChar) % HASHER;

    return (h < 0) ? h + HASHER : h;
  }
  // ---------------------
  // Rabin-Karp algorithm
  public static int indexOf_rk(String t, String p) {
    int hashValue = firstFootprint(t, p.length());
    int hashValuePattern = firstFootprint(p, p.length());
    int coef = 1;

    for (int i = 0; i < p.length()-1; i++) {
      coef = (coef * BASE) % HASHER;
    }

    if (t.length() == p.length() && hashValue == hashValuePattern) {
      return 0;
    }

    for (int i = 0; i <= t.length()-p.length(); i++) {
      if (hashValue == hashValuePattern) {
        for (int j = 0; j < p.length(); j++) {
          if (t.charAt(i+j) == p.charAt(j) && j + 1 == p.length()) {
            return i;
          } else if (t.charAt(i+j) != p.charAt(j)) {
            break;
          }
        }
      }
      if (i < t.length()-p.length()) {
        hashValue = nextFootprint(hashValue, t.charAt(i), t.charAt(i + p.length()), coef);
      }
    }

    return -1;
  }
}
