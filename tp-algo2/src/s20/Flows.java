package s20;
import java.util.*;

public class Flows {
  //==================================================
  public static class MaxFlowResult {
    public WeightedDiGraph flow;
    public int       throughput;
    public boolean[] isInSourceGroup; // gives the Min-Cut solution
  }
  //==================================================
  public static MaxFlowResult maxFlow(WeightedDiGraph capacity, int source, int sink) {
    WeightedDiGraph resid = initialResid(capacity);
    WeightedDiGraph flow  = initialFlow (capacity);
    
    while (true) {
      List<Integer> p = resid.pathBetween(source, sink);
      if (p == null) break;
      updateFlow(flow, resid, p, minStep(resid, p));
    }
    
    suppressEmptyEdges(flow);
    MaxFlowResult flowResult = new MaxFlowResult();
    flowResult.flow = flow;
    flowResult.isInSourceGroup = new boolean[capacity.nbOfVertices()];
    
    for (Integer vid : flow.neighboursFrom(source)) {
      flowResult.throughput += flow.edgeWeight(source, vid);
    }
    for (int vid = 0; vid < capacity.nbOfVertices(); vid++) {
      flowResult.isInSourceGroup[vid] = null != resid.pathBetween(source, vid);
    }
    return flowResult;
  }

  public static void suppressEmptyEdges(WeightedDiGraph g) {
    for(WeightedDiGraph.Edge e : g.allEdges()) {
      if (e.weight == 0) g.removeEdge(e.from, e.to);
    }
  }

  public static int minStep(WeightedDiGraph g, List<Integer> path ) {
    int minStep = Integer.MAX_VALUE;
    for (int i = 0; i < path.size() - 1; i++) {
      minStep = Math.min(minStep, g.edgeWeight(path.get(i), path.get(i + 1)));
    }
    return minStep;
  }

  public static WeightedDiGraph initialResid(WeightedDiGraph cap) {
    WeightedDiGraph initialResid = new WeightedDiGraph(cap);
    suppressEmptyEdges(initialResid);
    return initialResid;
  }

  public static WeightedDiGraph initialFlow(WeightedDiGraph cap) {
    List<WeightedDiGraph.Edge> edges = new LinkedList<>();
    for (WeightedDiGraph.Edge e : cap.allEdges()) {
      edges.add(new WeightedDiGraph.Edge(e.from, e.to, 0));
      edges.add(new WeightedDiGraph.Edge(e.to, e.from, 0));
    }
    return new WeightedDiGraph(cap.nbOfVertices(), edges);
  }

  public static void updateFlow(WeightedDiGraph flow, WeightedDiGraph resid, List<Integer> path, int benefit) {
    for(int i = 0; i < path.size() - 1; i++) {
      int from = path.get(i), to = path.get(i + 1);
      int newW = resid.edgeWeight(from, to) - benefit;
      addCost(resid, from, to, newW);
  
      if (resid.isEdge(to, from)) {
        newW = resid.edgeWeight(to, from) + benefit;
      }
      else {
        newW = benefit;
      }
      addCost(resid, to, from, newW);
      
      newW = flow.edgeWeight(from, to) + benefit;
      addCost(flow, from, to, newW);
      
      if (flow.isEdge(to, from)) {
        newW = flow.edgeWeight(to, from) - benefit;
      }
      else {
        newW = -benefit;
      }
      addCost(flow, to, from, newW);
    }
  }

  private static void addCost(WeightedDiGraph g, int from, int to, int delta) {
    g.removeEdge(from, to);
    if (delta != 0) g.putEdge(from, to, delta);
  }
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  // ------------------------------------------------------------
  public static void main(String [] args) {
    int nVertices = 6;  //int nEdges = 12;
    final int A=0, B=1, C=2, D=3, E=4, F=5;
    int    [] srcs  = {A, A, A, B, B, D, D, D, D, E, F, F };
    int    [] dsts  = {B, C, F, F, C, A, B, C, E, A, D, E };
    int    [] costs = {12,6, 14,1, 7, 9, 3, 2, 4, 5, 10,11};
    
    WeightedDiGraph g = new WeightedDiGraph(nVertices, srcs, dsts, costs);
    System.out.println("                Input Graph: " + g);
    
    int source=0, sink=2;
    System.out.println("              Source vertex: "+ source);
    System.out.println("                Sink vertex: "+ sink);
    MaxFlowResult result = maxFlow(g, source, sink);
    System.out.println("               Maximal flow: "+ result.flow);
    System.out.println("           Total throughput: "+ result.throughput);
    System.out.println("Source-group in the min-cut: "+ groupFromArray(result.isInSourceGroup));    
  }
  
  static List<Integer> groupFromArray(boolean[] t) {
    List<Integer> res=new LinkedList<>();
    for(int i=0; i<t.length; i++)
      if (t[i]) res.add(i);
    return res;
  }

}
