package s22;
import java.awt.Point;
import java.util.Arrays;
import java.util.TreeSet;
import java.util.SortedSet;
import java.util.List;

//============================================================
public class ClosestPair {
  private static Point crtSolA;
  private static Point crtSolB;
  private static double crtMinDist;
  private static Point[] pt;
  private static int leftMostCandidateIndex;
  //private static BST candidates;   // problem: Point is not Comparable
  private static TreeSet<Point> candidates;
//  public static double nbPointsInRange;
//  public static double timesOfSubsets;
//  public static double highestAmountOfPtsInRange;
  

  // ------------------------------------------------------------
  public static Point[] closestPair(Point[] points) {
    pt = new Point[points.length];
    System.arraycopy(points, 0, pt, 0, points.length);
    findClosestPair();
    return (new Point [] {crtSolA,crtSolB});
  }
  // ------------------------------------------------------------
  public static Point[] closestPair(List<? extends Point> points) {
    pt = new Point[points.size()];
    for (int i=0; i<points.size(); i++) {
      Point p = points.get(i);
      pt[i]= p;// new Point(p);
    }
    findClosestPair();
    return (new Point[] {crtSolA, crtSolB});
  }
  // ------------------------------------------------------------
  // IN : points in global array pt
  // OUT: global variables       crtSolA, crtSolB 
  
  private static void findClosestPair() {
    // -- plane sweep  (discrete simulation)
    Arrays.sort(pt, PointComparator.HORIZONTALLY);
    candidates = new TreeSet<>(PointComparator.VERTICALLY);
    crtSolA = pt[0];
    crtSolB = pt[1];
    crtMinDist = pt[1].distance(pt[0]);
    leftMostCandidateIndex = 0;
  
    for (Point p : pt){
      handleEvent(p);
    }
  }
  // ------------------------------------------------------------
  private static void handleEvent(Point p) {
    // consider using: candidates.subSet(pmin, pmax)
    shrinkCandidates(p);
    Point pMax = new Point(p.x, p.y + (int) crtMinDist);
    Point pMin = new Point(p.x - (int) crtMinDist, p.y - (int) crtMinDist);
    SortedSet<Point> neighbors = candidates.subSet(pMin, pMax);
    
//    Ex.2
//    nbPointsInRange += neighbors.size();
//    timesOfSubsets++;
//    if (highestAmountOfPtsInRange < neighbors.size()) highestAmountOfPtsInRange = neighbors.size();
  
  
    double dist;
    for (Point candidate : neighbors) {
      dist = candidate.distance(p);
      if (dist < crtMinDist) {
        crtMinDist = dist;
        crtSolA = p;
        crtSolB = candidate;
      }
    }
    candidates.add(p);
  }
  
  private static void shrinkCandidates(Point p) {
    while (p.x-pt[leftMostCandidateIndex].x > crtMinDist) {
      candidates.remove(pt[leftMostCandidateIndex]);
      leftMostCandidateIndex++;
    }
  }
}
