package s04;
// =======================
public class BST<E extends Comparable<E>> {
  protected BTree<E> tree;
  protected int   crtSize;

  public BST() {
    tree = new BTree<E>();
    crtSize = 0;
  }

  public BST(E [] tab) {  // PRE sorted, no duplicate
    this();
    if (tab.length > 0) {
      this.tree = optimalBST(tab, 0, tab.length - 1);
    }

  }

  /** returns where e is, or where it should be inserted as a leaf */
  protected  BTreeItr<E> locate(E e) {
    if (this.crtSize == 0) {

      return this.tree.root();
    }

    boolean hasFinished = false;
    BTreeItr<E> currentPlace = this.tree.root();


    while (!hasFinished) {
      if (currentPlace.consult().equals(e)) {
        return currentPlace;
      }

      if (currentPlace.consult().compareTo(e) > -1) {
        if (!currentPlace.hasLeft() || currentPlace.consult().equals(e)) {

          return currentPlace.left();
        } else {
          currentPlace = currentPlace.left();
        }
      } else {
        if (!currentPlace.hasRight() || currentPlace.consult().equals(e)) {

          return currentPlace.right();
        } else {
          currentPlace = currentPlace.right();
        }
      }
    }

    return currentPlace;
  }

  public void add(E e) {
    if (contains(e)) {
      return;
    }

    locate(e).insert(e);
    crtSize++;
  }

  public void remove(E e) {

    if (!locate(e).isBottom()) {
      while (locate(e).hasLeft()) {
        locate(e).rotateRight();
      }
      crtSize--;
      locate(e).paste(locate(e).right().cut());
    }
  }

  public boolean contains(E e) {
    BTreeItr<E> ti = locate(e);
    return ! ti.isBottom();
  }

  public int size() {
    return crtSize;
  }

  public boolean isEmpty() {
    return size() == 0;
  }

  public E minElt() {
    BTreeItr<E> currentPlace = tree.root();
    while (true) {
      if (currentPlace.hasRight()) {
        currentPlace = currentPlace.right();
      } else {

        return currentPlace.consult();
      }
    }
  }

  public E maxElt() {
    BTreeItr<E> currentPlace = tree.root();
    while (true) {
      if (currentPlace.hasLeft()) {
        currentPlace = currentPlace.left();
      } else {

        return currentPlace.consult();
      }
    }
  }

  @Override public String toString() {
    return ""+tree;
  }

  public String toReadableString() {
    String s = tree.toReadableString();
    s += "size=="+crtSize+"\n";
    return s;
  }

  // --------------------------------------------------
  // --- Non-public methods
  // --------------------------------------------------

  private BTree<E> optimalBST(E[] sorted, int left, int right) {
    BTree<E> r = new BTree<E>();
    BTreeItr<E> ri = r.root();

    if (right-left == 0) {
      ri.insert(sorted[left]); // or right
      crtSize++;

      return r;
    } else if (right-left < 0) {
      return r;
    }

    ri.insert(sorted[(right - left) / 2 + left]);
    crtSize++;

    ri.left().paste(optimalBST(sorted, left, (right-left)/2+left-1));
    ri.right().paste(optimalBST(sorted,(right-left)/2+left+1, right));

    return r;
  }
}
