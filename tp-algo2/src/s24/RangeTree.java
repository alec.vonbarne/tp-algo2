package s24;
import java.awt.Point;
import java.util.*;

public class RangeTree {
  private static final Comparator<Point> VERTICALLY =
      Comparator.<Point>comparingInt(a->a.y).thenComparingInt(a->a.x);
  private static final Comparator<Point> HORIZONTALLY =
      Comparator.<Point>comparingInt(a->a.x).thenComparingInt(a->a.y);
  //============================================================
  /*
  // Instead of this class, we can use a lambda expression...
  static class MyIndexComparator implements Comparator<Integer> {
    Point[] ptsArr;
  
    public  MyIndexComparator(Point[] myArray) {
      this.ptsArr = myArray;
    }
    @Override public int compare(Integer a, Integer b) {
      return VERTICALLY.compare(ptsArr[a], ptsArr[b]);
    }
  }
  */
  // ============================================================
  @SuppressWarnings("serial")
  static class  BSTElt extends Point implements Comparable<BSTElt> {
    public BSTElt(Point a) {super(a);}
    @Override public int compareTo(BSTElt b) {
      return VERTICALLY.compare(this, b);
    }
  }
  // ============================================================
  static class RangeTreeElt {
    final BST<BSTElt> yTree;
    final Point       xMedian;
    public RangeTreeElt(BST<BSTElt> t, Point x) {
      yTree = t; xMedian = x;
    }
    @Override public String toString() {
      return ""+xMedian.x;
    }
  }
  // ============================================================
  // ============================================================
  protected final BTree<RangeTreeElt> xTree;  // elts of type RangeTreeElt

  public RangeTree(Point [] points) {
    int n = points.length;
    Point[] xP = new Point[n];
    for (int i = 0; i < n; i++) xP[i] = new BSTElt(points[i]);
    Arrays.sort(xP, HORIZONTALLY);
    List<Integer> yOrder = new ArrayList<>(n);
  
    for (int i = 0; i < n; i++) {
      yOrder.add(i);
    }
    yOrder.sort(Comparator.comparingInt(a -> xP[(int) a].y).thenComparingInt(a -> xP[(int) a].x));
    
    xTree = build2dRangeTree(xP, 0, n - 1, yOrder);
  }

  public Point[] search(Point bottomLeft, Point topRight) {
    List<Point> v = query2D(xTree.root(), bottomLeft, topRight,
                            Integer.MIN_VALUE, Integer.MAX_VALUE);
    return v.toArray(new Point[0]);
  }

  public String toString() {
    return ""+xTree.toReadableString();
  }

  // ------------------------------------------------------------
  // --- Non-public methods
  // ------------------------------------------------------------
  private static List<Point> query1D(BST<BSTElt> ty, int yFrom, int yTo) {
    BSTElt pMin = new BSTElt(new Point(Integer.MIN_VALUE, yFrom));
    BSTElt pMax = new BSTElt(new Point(Integer.MAX_VALUE, yTo  ));
    return new LinkedList<>(ty.inRange(pMin, pMax));
  }

  private static List<Point> query2D(BTreeItr<RangeTreeElt> tx,
                 Point bottomLeft, Point topRight, int xMin, int xMax) {
    int xFrom = bottomLeft.x; int xTo = topRight.x;
    int yFrom = bottomLeft.y; int yTo = topRight.y;
    List<Point> v = new ArrayList<Point>();
    if (tx.isBottom()) return v;
    RangeTreeElt m = tx.consult();
    int mx = m.xMedian.x; int my = m.xMedian.y;

    if (mx > xTo) {
      v.addAll(query2D(tx.left(), bottomLeft, topRight, xMin, mx));
      return v;
    } else if (mx < xFrom) {
      v.addAll(query2D(tx.right(), bottomLeft, topRight, mx, xMax));
      return v;
    }

    if (xMin >= xFrom && xMax <= xTo) {
      v.addAll(query1D(m.yTree, yFrom, yTo));
      return v;
    }

    if (my >= yFrom && my <= yTo)
      v.add(m.xMedian);

    v.addAll(query2D(tx.left(), bottomLeft, topRight, xMin, mx));
    v.addAll(query2D(tx.right(), bottomLeft, topRight, mx, xMax));

    return v;
  }

  private static BTree<RangeTreeElt> build2dRangeTree(Point[] xP,
                                        int left, int right,
                                        List<Integer> yOrder) {
    int n = right+1-left;
    BTree<RangeTreeElt> t = new BTree<RangeTreeElt>();
    BTreeItr<RangeTreeElt> ti=t.root();
    if (n == 0) return t;
    BSTElt[] yElts = new BSTElt[n];
    for (int i=0; i<n; i++)
      yElts[i] = new BSTElt((xP[yOrder.get(i)]));
    BST<BSTElt> ty = new BST<BSTElt>(yElts);
    int mid = (left+right)/2;

    List<Integer> yLeftOrder = new ArrayList<>(mid - left);
    List<Integer> yRightOrder = new ArrayList<>(right - mid);
    for (int i = 0; i < n; i++) {
      int y = yOrder.get(i);
      if (y < mid) {
        yLeftOrder.add(y);
      } else if (y > mid) {
        yRightOrder.add(y);
      }
    }
  
    yLeftOrder.sort(Comparator.comparingInt(a -> xP[(int) a].y).thenComparingInt(a -> xP[(int) a].x));
    yRightOrder.sort(Comparator.comparingInt(a -> xP[(int) a].y).thenComparingInt(a -> xP[(int) a].x));
    ti.insert(new RangeTreeElt(ty, xP[mid]));
    ti.left().paste(build2dRangeTree(xP, left, mid-1, yLeftOrder));
    ti.right().paste(build2dRangeTree(xP, mid+1, right, yRightOrder));

    return t;
  }

  /*
  private static List<Integer> yorder(Point[] pts) {
    List<Integer> yorder = new ArrayList<>();

    Map<Point, Integer> indexes = new HashMap<>(pts.length);

    for (int i = 0; i < pts.length; i++) {
      indexes.put(pts[i], i);
    }

    Point[] yP = pts.clone();
    Arrays.sort(yP, VERTICALLY);

    for (int i = 0; i < pts.length; i++) {
      yorder.add(indexes.get(yP[i]));
    }

    return yorder;
  }
  */
}
