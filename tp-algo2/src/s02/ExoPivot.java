package s02;

import java.util.Random;

public class ExoPivot {
    static boolean isOk(int[] t) {
        Random random = new Random();
        int n=t.length;
        int r = random.nextInt(n-1);
        System.out.printf("The pivot: %d\n", r);
        while (true) {
            if(n==1) return r==0;
            if(r==0) return t[r] <= t[r+1];
            if(r==n-1) return t[r] <= t[r-1];
            if (t[r] <= t[r+1] && t[r] <= t[r-1]) {
                System.out.printf("Found at: %d\n", r);
                return true;
            }
            r--;
        }
    }
    
    static void printArray(int[] array) {
        System.out.print("{");
        for (int i = 0; i < array.length-1; i++) {
            System.out.print(array[i] + ",");
        }
        System.out.println(array[array.length-1]+"}");
    }
    
    static int[] createRandomArray() { // Damien: Get the code from Internet
        Random rd = new Random(); // creating Random object
        int[] arr = new int[1000];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = rd.nextInt(); // storing random integers in an array
        }
        return arr;
    }
    
    public static void main(String[] args) {
        int[] testArray = createRandomArray();
        printArray(testArray);
        System.out.printf("Is this array OK ?  -> %s", isOk(testArray));
    }
}
