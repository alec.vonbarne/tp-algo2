package s02;

public class ExoStars {
    static void printArray(int[] array) {
        System.out.print("{");
        for (int i = 0; i < array.length-1; i++) {
            System.out.print(array[i] + ",");
        }
        System.out.println(array[array.length-1]+"}");
    }
    
    static int[] setStars(int[] array) {
        assert array.length > 1;
        int formerCell = array[0];
        int[] stars = new int[array.length];
        
        for (int i = 1; i <= array.length-1; i++) {
            if (formerCell < array[i]) {
                stars[i] += 1;
            }
            stars[i] += 1;
            formerCell = array[i];
        }
    
        formerCell = array[array.length-1];
        int formerStar = stars[array.length-1];
        for (int i = array.length-2; i >= 0; i--) {
            if (array[i] >= formerCell) {
                stars[i] = formerStar;
                if (array[i] > formerCell) {
                    stars[i] += 1;
                }
            }
            formerCell = array[i];
            formerStar = stars[i];
        }
        
        return stars;
    }
    
    static int maxStars(int[] stars) {
        int maxStar = stars[0];
        for (int star : stars) {
            if (star > maxStar) {
                maxStar = star;
            }
        }
        return maxStar;
    }
    
    static void printStars(int[] array) {
        int[] stars = setStars(array);
        int maxStar = maxStars(stars);
        for (int i = 0; i <= maxStar; i++) {
            for (int n = 0; n < stars.length; n++) {
                if (i == 0 || stars[n] != 0) {
                    System.out.print(" *");
                    stars[n]--;
                } else {
                    System.out.print("  ");
                }
            }
            System.out.println();
        }
    }
    
    public static void main(String[] args) {
        int[] testArray     = {4,2,2,8,8,3,1,5};
        printArray(testArray);
        printStars(testArray);
    }
}
