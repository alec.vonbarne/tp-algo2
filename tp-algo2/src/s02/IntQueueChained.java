package s02;

import java.util.Arrays;

public class IntQueueChained {

  private static final int NIL = -1;
  private static int[] next = {NIL};
  private static int[] elts = new int[1];
  private static int firstFreeCell = 0;

  //======================================================================
  private int front = NIL;
  private int back = NIL;

  // ------------------------------
  IntQueueChained() {
  }

  private int allocate() {
    if (firstFreeCell == NIL) {
      firstFreeCell = next.length;

      elts = Arrays.copyOf(elts, elts.length * 2);
      next = Arrays.copyOf(next, next.length * 2);

      for (int i = firstFreeCell; i < next.length - 1; i++) {
        next[i] = i + 1;
      }
      next[next.length - 1] = NIL;
    }
    int i = firstFreeCell;
    firstFreeCell = next[i];
    next[i] = NIL;
    return i;
  }

  private void deallocate(int i) {
    next[i] = firstFreeCell;
    firstFreeCell = i;
  }


  // --------------------------
  void enqueue(int elt) {
    if (back == NIL) {
      createList(elt);
    } else {
      int n = allocate();
      next[back] = n;
      back = n;
      elts[back] = elt;
      next[back] = NIL;
    }
  }

  private void createList(int elt) {
    int n = allocate();
    elts[n] = elt;
    next[n] = NIL;
    back = n;
    front = n;
  }

  // --------------------------
  public boolean isEmpty() {
    return back == NIL;
  }

  // --------------------------
  public int consult() {
    return elts[front];
  }

  // --------------------------
  public int dequeue() {
    int f = front;
    int e = consult();

    if (front == back) {
      back = NIL;
      front = NIL;
    } else {
      front = next[front];
    }
    deallocate(f);

    return e;
  }
}
