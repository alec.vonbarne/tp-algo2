package s25;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;

public class IntervalTree {
  //============================================================
  protected static class IntervTreeElt {
    public final Interval[] leftSorted;
    public final Interval[] rightSorted;
    public final int xMid;

    public IntervTreeElt (List<Interval> p, int midValue) {
      this.xMid = midValue;

      int n = p.size();
      this.leftSorted = new Interval[n];
      this.rightSorted = new Interval[n];

      if (n == 0) {
        return;
      }

      p.sort(Interval.STARTING);

      for (int i = 0; i < p.size(); i++) {
          leftSorted[i] = p.get(i);
      }

      p.sort(Interval.ENDING);

      for (int i = 0; i < p.size(); i++) {
        rightSorted[i] = p.get(i);
      }
    }

    public int midPoint() { return xMid; }

    public List<Interval> intersecting(int x) {
      assert (rightSorted.length == leftSorted.length);

      List<Interval> intersecting = new ArrayList<>();

      if (x == midPoint()) {
        intersecting = Arrays.asList(leftSorted);
      } else if (x < midPoint()) {
        for (Interval i : leftSorted) {
          if (i.start > x) {
            break;
          }
          intersecting.add(i);
        }
      } else {
        for (int i = rightSorted.length - 1; i >= 0; i--) {
          Interval interval = rightSorted[i];

          if (interval.end < x) {
            break;
          }

          intersecting.add(interval);
        }
      }

      return intersecting;
    }

    @Override public String toString() {
      return xMid+"("+leftSorted.length+")";
    }
  }
  //============================================================
  //============================================================
  protected final BTree<IntervTreeElt> tree;

  public IntervalTree(Interval[] p) {
    tree = fromIntervals(p);
  }

  public Interval[] containing(int x) {
    List<Interval> res = intersecting(tree.root(), x);
    return res.toArray(new Interval[0]);
  }

  @Override public String toString() {
    return tree.toReadableString();
  }
  // ----------------------------------------------------------------------
  // --- Non public methods
  // ----------------------------------------------------------------------
  private static List<Interval> intersecting(BTreeItr<IntervTreeElt> r, int x) {
    if (r == null || r.isBottom()) return new ArrayList<>();

    IntervTreeElt elt = r.consult();
    List<Interval> intersecting = elt.intersecting(x);

    if (x < elt.midPoint() && r.hasLeft()) intersecting.addAll(intersecting(r.left(), x));
    else if (x > elt.midPoint() && r.hasRight()) intersecting.addAll(intersecting(r.right(), x));

    return intersecting;
  }

  private static BTree<IntervTreeElt> fromIntervals(Interval[] p) {
    BTree<IntervTreeElt> tree = new BTree<>();

    if (p.length == 0) return tree;

    List<Integer> xt = new ArrayList<>();

    for (Interval it : p) {
      xt.add(it.start);
      xt.add(it.end);
    }

    Collections.sort(xt);
    int xMid = xt.get(p.length);

    List<Interval> midPart = new ArrayList<>();
    List<Interval> leftPart = new ArrayList<>();
    List<Interval> rightPart = new ArrayList<>();

    for (Interval i : p) {
      if (i.start <= xMid && xMid <= i.end) midPart.add(i);
      else if (i.end < xMid) leftPart.add(i);
      else rightPart.add(i);
    }

    BTreeItr<IntervTreeElt> itr = tree.root();
    itr.insert(new IntervTreeElt(midPart, xMid));
    if (leftPart.size() > 0) itr.left().paste(fromIntervals(leftPart.toArray(new Interval[0])));
    if (rightPart.size() > 0) itr.right().paste(fromIntervals(rightPart.toArray(new Interval[0])));

    return tree;
  }
}
