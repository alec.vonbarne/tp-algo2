package s08;

public class SplayTree<K extends Comparable<K>> {
  BTree<K> tree = new BTree<K>();
  int crtSize = 0;
  // --------------------------------------------------
  public SplayTree() { super(); }
  // --------------------------------------------------
  public void add(K e) {
    if (contains(e)) return;  // This "splays" the tree!
    crtSize++;

    BTreeItr itr = locate(e);

    if (size() == 1) {
      itr.insert(e);

      return;
    }

    itr.insert(e);

    splayToRoot(itr);
  }
  // --------------------------------------------------
  public void remove(K e) {
    if (! contains(e)) return; // This "splays" the tree!
    crtSize--;
    if (tree.root().hasLeft() && tree.root().hasRight()) {
      BTree<K> oldRight = tree.root().right().cut();
      tree=tree.root().left().cut();
      BTreeItr<K> maxInLeft= tree.root().rightMost().up();
      BTreeItr<K> ti=splayToRoot(maxInLeft); // now tree has no right subtree!
      ti.right().paste(oldRight);
    } else {  // the tree has only one child
      if (tree.root().hasLeft()) tree=tree.root().left() .cut();
      else                       tree=tree.root().right().cut();
    }
  }
  // --------------------------------------------------
  public boolean contains(K e) {
    if (isEmpty()) return false;
    BTreeItr<K> ti=locate(e);
    boolean absent=ti.isBottom();
    if (absent) ti=ti.up();
    ti=splayToRoot(ti);
    return !absent;
  }
  // --------------------------------------------------
  protected  BTreeItr<K> locate(K e) {
    BTreeItr<K> ti = tree.root();
    while(!ti.isBottom()) {
      K c = ti.consult();
      if (e.compareTo(c)==0) break;
      if (e.compareTo(c)< 0) ti = ti.left();
      else                   ti = ti.right();
    }
    return ti;
  }
  // --------------------------------------------------
  public int     size()    { return crtSize;}
  // --------------------------------------------------
  public boolean isEmpty() { return size() == 0;}
  // --------------------------------------------------
  public K    minElt() {
    BTreeItr itr = tree.root();
    while (itr.hasRight()) {
      itr = itr.right();
    }

    return (K) itr.consult();
  }
  // --------------------------------------------------
  public K    maxElt() {
    BTreeItr itr = tree.root();
    while (itr.hasLeft()) {
      itr = itr.left();
    }

    return (K) itr.consult();
  }
  // --------------------------------------------------
  public String toString() {
    return ""+tree.toReadableString()+"SIZE:"+size();
  }
  // --------------------------------------------------
  // --- Non-public methods
  // --------------------------------------------------
  // PRE:     ! ti.isBottom()
  // RETURNS: root position
  // WARNING: ti is no more valid afterwards
  private BTreeItr<K> splayToRoot(BTreeItr<K> ti) {
    if (ti.isRoot()) {

      return ti;
    }

    if (ti.up().isRoot()) {

      return applyZig(ti);
    }

    if (ti.isLeftArc() == ti.up().isLeftArc()) {

      return splayToRoot(applyZigZig(ti));
    }

    return splayToRoot(applyZigZag(ti));
  }
  // --------------------------------------------------
  // PRE / RETURNS : Zig situation (see schemas)
  // WARNING: ti is no more valid afterwards
  private BTreeItr<K> applyZig(BTreeItr<K> ti) {
    boolean isLeftArc = ti.isLeftArc();
    ti=ti.up();

    if (isLeftArc) {
      ti.rotateRight();
    } else {
      ti.rotateLeft();
    }

    return ti;
  }
  // --------------------------------------------------
  // PRE / RETURNS : ZigZig situation (see schemas)
  // WARNING: ti is no more valid
  private BTreeItr<K> applyZigZig(BTreeItr<K> ti) {
    boolean isLeftArc = ti.isLeftArc();

    ti = applyZig(ti);
    ti = applyZig(ti);

    //BTreeItr<K> finalTi = ti;
    if (isLeftArc) {
      applyZig(ti.right().left());
    } else {
      applyZig(ti.left().right());
    }

    return ti;
  }
  // --------------------------------------------------
  // PRE / RETURNS : ZigZag situation (see schemas)
  // WARNING: ti is no more valid
  private BTreeItr<K> applyZigZag(BTreeItr<K> ti) {

    return applyZig(applyZig(ti));
  }
  // --------------------------------------------------
}
