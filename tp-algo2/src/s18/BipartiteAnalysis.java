package s18;

import java.util.List;
import java.util.ArrayList;

public class BipartiteAnalysis {
  private final UGraph    graph;
  private boolean         isBipartite;
  private final boolean[] isVisited;
  private final boolean[] isWhite;  // meaningful if isBipartite
  private final int[]     parent;   // the traversal tree structure
  private List<Integer>   oddCycle; // meaningful if !isBipartite
  
  public BipartiteAnalysis(UGraph g) {
    int n=g.nbOfVertices();
    graph=g;
    isVisited=new boolean[n];
    isWhite=new boolean[n];
    parent=new int[n];
    
    // ... TODO: prepare and drive the depth-first traversal
    isBipartite = true;
    for (int i = 0; i < n; i++) {
      if (!isVisited[i]) {
        parent[i] = -1;
        dft(i);
        
        if (!isBipartite) break;
      }
    }
  }
  
  // Depth-first traversal
  // PRE: vid is not visited, but has been assigned a color
  // POST: either the whole subtree has been colored,
  //           or an odd cycle has been found via a back edge
  //              (meaning the graph is not bipartite).
  // Caution: exit the traversal once you find an "odd-cycle";
  //          a "back edge" is later met as a "forward" one...
  private void dft(int vid) {
    isVisited[vid] = true;
    for (Integer neighbourVid : graph.neighboursOf(vid)) {
      if (!isVisited[neighbourVid]) {
        isWhite[neighbourVid] = !isWhite[vid];
        parent[neighbourVid] = vid;
        dft(neighbourVid);
        
        if (!isBipartite) return;
      } else if (isWhite[neighbourVid] == isWhite[vid]) {
        isBipartite = false;
        rememberOddCycle(neighbourVid, vid);
        
        return;
      }
    }
  }
  
  // build the oddCycle list with the tree path: <descendantId, ..., ancestorId>
  private void rememberOddCycle(int ancestorId, int descendantId) {
    // TODO: A COMPLETER... - Done
    oddCycle = new ArrayList<>();
    int actualId = descendantId;
    while (actualId != ancestorId) {
      oddCycle.add(actualId);
      actualId = parent[actualId];
    }
    oddCycle.add(ancestorId);
  }

  
  public boolean isBipartite() {
    return isBipartite;
  }
  
  public int colorOf(int vid) {
    assert isBipartite();
    return isWhite[vid] ? 0 : 1;
  }
  
  public List<Integer> anOddCycle() {
    assert !isBipartite();
    return new ArrayList<Integer>(oddCycle);
  }
}
