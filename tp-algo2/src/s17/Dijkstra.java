package s17;
import s03.PtyQueue;

import java.util.Arrays;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Set;

// ------------------------------------------------------------
public class Dijkstra {
  // ------------------------------------------------------------
  static class Vertex implements Comparable<Vertex> {
    public final int vid;
    public final int pty;
    public Vertex(int vid, int pty) {
      this.vid=vid; this.pty=pty;
    }
    @Override public int compareTo(Vertex v) {
      return Integer.compare(this.pty,  v.pty);
    }
  }
  // ------------------------------------------------------------
  // POST : minDist[i] is the min distance from a to i
  //                      MAX_VALUE if i is not reachable, 0 for a
  //         parent[i] is the parent of i in the corresponding tree
  //                      -1 if i is not reachable, and for a
  public static void dijkstra(WeightedDiGraph g, int a,
                              int[] minDist, int[] parent) {
  
    boolean[] isVisited = new boolean[minDist.length];
    
    for (int i=0; i<minDist.length; i++){
      minDist[i] = Integer.MAX_VALUE;
      parent[i] = -1;
    }
    
    minDist[a] = 0;

    PriorityQueue<Vertex> pq = new PriorityQueue<>();

    pq.add(new Vertex(a, minDist[a]));

    while (!pq.isEmpty()) {
      int k = pq.poll().vid;
      if (isVisited[k]) continue;
      
      isVisited[k] = true;
      for (int i : g.neighboursFrom(k)) {
        if (minDist[k] + g.edgeWeight(k, i) < minDist[i]) {
          minDist[i] = minDist[k] + g.edgeWeight(k, i);
          parent[i] = k;
          isVisited[i] = false;
          pq.add(new Vertex(i, minDist[i]));
        }
      }
    }
  }
  
  // ------------------------------------------------------------
  // POST : minDist[i] is the min distance from a to i
  //                      MAX_VALUE if i is not reachable, 0 for a
  //         parent[i] is the parent of i in the corresponding tree
  //                      -1 if i is not reachable, and for a
  public static void invertedDijkstra(WeightedDiGraph g, int a,
                              int[] minDist, int[] parent) {
    
    boolean[] isVisited = new boolean[minDist.length];
    
    for (int i=0; i<minDist.length; i++){
      minDist[i] = Integer.MAX_VALUE;
      parent[i] = -1;
    }
    
    minDist[a] = 0;
    
    PriorityQueue<Vertex> pq = new PriorityQueue<>();
    
    pq.add(new Vertex(a, minDist[a]));
    
    while (!pq.isEmpty()) {
      int k = pq.poll().vid;
      if (isVisited[k]) continue;
      
      isVisited[k] = true;
      for (int i : g.neighboursTo(k)) {
        if (minDist[k] + g.edgeWeight(i, k) < minDist[i]) {
          minDist[i] = minDist[k] + g.edgeWeight(i, k);
          parent[i] = k;
          isVisited[i] = false;
          pq.add(new Vertex(i, minDist[i]));
        }
      }
    }
  }
  // ------------------------------------------------------------
  /** returns all vertices in vid's strongly connected component */
  static Set<Integer> strongComponentOf(WeightedDiGraph g, int vid) {
    Set<Integer> res=new HashSet<>();
    res.add(vid);
    
    int size = g.nbOfVertices();
    
    int[] from  = new int[size];
    int[] to = new int[size];
    
    dijkstra(g, vid, new int[size], from);
    invertedDijkstra(g, vid, new int[size], to);
    
    for (int i = 0; i < size; i++){
      if (from[i] != -1 && to[i] != -1) res.add(i);
    }
    
    return res;
  }


  // ------------------------------------------------------------
  public static void main(String [] args) {
    int nVertices = 6; // int nEdges = 12;
    final int A=0, B=1, C=2, D=3, E=4, F=5;
    int[] srcs  = {A, A, A, B, B, D, D, D, D, E, F, F };
    int[] dsts  = {B, C, F, F, C, A, B, C, E, A, D, E };
    int[] costs = {12,6, 14,1, 7, 9, 3, 2, 4, 5, 10,11};

    WeightedDiGraph g = new WeightedDiGraph(nVertices, srcs, dsts, costs);
    System.out.println("Input Graph: " + g);

    int n = g.nbOfVertices();
    int[] minCost = new int[n];
    int[] parent  = new int[n];
    for (int a=0; a<n; a++) {
      dijkstra(g, a, minCost, parent);
      System.out.println("\nMinimal distances from " +a);
      for (int i=0; i<minCost.length; i++) {
        String s="to "+ i +":";
        if (minCost[i]==Integer.MAX_VALUE) s+=" unreachable";
        else s+= " total " + minCost[i] +", parent "+parent[i];
        System.out.println(s);
      }
    }
  }
}
